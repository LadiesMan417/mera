INC := $(UNI10_ROOT)/include
LIB := $(UNI10_ROOT)/lib
CC:=g++
DEBUG:=-g -fsanitize=address
FLAGS:=-O3 -m64 -std=c++11

main.e: main.o MERA.o
	$(CC) $(FLAGS) -I$(INC) -L$(LIB) $^ -o $@ -lblas -llapack -lm -luni10
main.o: main.cpp MERA.cpp MERA.h
	$(CC) $(FLAGS) -I$(INC) -c $<
MERA.o: MERA.cpp MERA.h
	$(CC) $(FLAGS) -I$(INC) -c $<

clean:
	rm -f *.o *.e
