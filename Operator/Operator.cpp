#include "Operator.h"
#include <string.h>
#include <stdio.h>

Operator::Operator(const char* optstr, float spin, int sites){
  char bufname[256];
  strcpy(bufname, optstr);
  char* pch;	
  pch = strtok(bufname, ", .");
  while (pch != NULL){
    if(std::string(pch) == "mx")
      opts[std::string(pch)] = matSx(spin);
    else if(std::string(pch) == "my")
      opts[std::string(pch)] = matSy(spin);
    else if(std::string(pch) == "mz")
      opts[std::string(pch)] = matSz(spin);
    else if(std::string(pch) == "m+")
      opts[std::string(pch)] = matSp(spin);
    else if(std::string(pch) == "m-")
      opts[std::string(pch)] = matSm(spin);
    pch = strtok(NULL, ", .");
  }
}

void Operator::spin_check(float spin){
  if(!(spin > 0 && floor(2 * spin) == 2 * spin)){
      std::ostringstream err;
      err<<"The given spin is not half integer.";
      throw std::runtime_error(err.str());
  }
}

uni10::Matrix Operator::matSp(float spin){
  spin_check(spin);
  int dim = spin * 2 + 1;
	/*
  if(dim > CURRENT_SUPPORTED_SPIN_DIM){
    std::ostringstream err;
    err<<"Spin = "<<spin <<" is not yet supported.";
    throw std::runtime_error(err.str());
  }
	*/
  if(dim == 2){
    double mat_elem[] = {\
      0, 1,\
      0, 0};
    return uni10::Matrix(dim, dim, mat_elem);
  }
  else if(dim == 3){
    double mat_elem[] = {\
      0.0, 1.0, 0.0,\
      0.0, 0.0, 1.0,\
      0.0, 0.0, 0.0};
    return sqrt(2) * uni10::Matrix(dim, dim, mat_elem);
  }
  else if(dim == 5){
    double mat_elem[] = {\
      0.0,     2.0,     0.0,     0.0, 0.0,\
      0.0,     0.0, sqrt(6),     0.0, 0.0,\
      0.0,     0.0,     0.0, sqrt(6), 0.0,\
      0.0,     0.0,     0.0,     0.0, 2.0,\
      0.0,     0.0,     0.0,     0.0, 0.0,};
    return uni10::Matrix(dim, dim, mat_elem);
  }
  return uni10::Matrix();
}

uni10::Matrix Operator::matSm(float spin){
  spin_check(spin);
  int dim = spin * 2 + 1;
	/*
  if(dim > CURRENT_SUPPORTED_SPIN_DIM){
    std::ostringstream err;
    err<<"Spin = "<<spin <<" is not yet supported.";
    throw std::runtime_error(err.str());
  }
	*/
  if(dim == 2){
    double mat_elem[] = {\
      0, 0,\
      1, 0};
    return uni10::Matrix(dim, dim, mat_elem);
  }
  else if(dim == 3){
    double mat_elem[] = {\
      0.0, 0.0, 0.0,\
      1.0, 0.0, 0.0,\
      0.0, 1.0, 0.0};
    return sqrt(2) * uni10::Matrix(dim, dim, mat_elem);
  }
  else if(dim == 5){
    double mat_elem[] = {\
      0.0,     0.0,     0.0,     0.0, 0.0,\
      2.0,     0.0,     0.0,     0.0, 0.0,\
      0.0, sqrt(6),     0.0,     0.0, 0.0,\
      0.0,     0.0, sqrt(6),     0.0, 0.0,\
      0.0,     0.0,     0.0,     2.0, 0.0,};
    return uni10::Matrix(dim, dim, mat_elem);
  }
  return uni10::Matrix();
}

uni10::Matrix Operator::matSx(float spin){
  spin_check(spin);
  int dim = spin * 2 + 1;
	/*
  if(dim > CURRENT_SUPPORTED_SPIN_DIM){
    std::ostringstream err;
    err<<"Spin = "<<spin <<" is not yet supported.";
    throw std::runtime_error(err.str());
  }
	*/
  if(dim == 2){
    double mat_elem[] = {\
      0,   0.5,\
      0.5, 0  };
    return uni10::Matrix(dim, dim, mat_elem);
  }
  else if(dim == 3){
    double mat_elem[] = {\
      0.0, 1.0, 0.0,\
      1.0, 0.0, 1.0,\
      0.0, 1.0, 0.0};
    return (1.0 / sqrt(2)) * uni10::Matrix(dim, dim, mat_elem);
  }
  else if(dim == 5){
    double mat_elem[] = {\
      0.0,     2.0,     0.0,     0.0, 0.0,\
      2.0,     0.0, sqrt(6),     0.0, 0.0,\
      0.0, sqrt(6),     0.0, sqrt(6), 0.0,\
      0.0,     0.0, sqrt(6),     0.0, 2.0,\
      0.0,     0.0,     0.0,     2.0, 0.0,};
    return 0.5 * uni10::Matrix(dim, dim, mat_elem);
  }
  return uni10::Matrix();
}

uni10::Matrix Operator::matSy(float spin){
  spin_check(spin);
  int dim = spin * 2 + 1;
	/*
  if(dim > CURRENT_SUPPORTED_SPIN_DIM){
    std::ostringstream err;
    err<<"Spin = "<<spin <<" is not yet supported.";
    throw std::runtime_error(err.str());
  }
	*/
  if(dim == 2){
    double mat_elem[] = {\
      0, 1,\
     -1, 0};
    return std::complex<double>(0,1./2.0)*uni10::Matrix(dim, dim, mat_elem);
  }
  else if(dim == 3){
    double mat_elem[] = {\
      0.0, 1.0, 0.0,\
     -1.0, 0.0, 1.0,\
      0.0,-1.0, 0.0};
    return std::complex<double>(0,1./sqrt(2.0))* uni10::Matrix(dim, dim, mat_elem);
  }
  else if(dim == 5){
    double mat_elem[] = {\
      0.0,     2.0,     0.0,     0.0, 0.0,\
     -2.0,     0.0, sqrt(6),     0.0, 0.0,\
      0.0,-sqrt(6),     0.0, sqrt(6), 0.0,\
      0.0,     0.0,-sqrt(6),     0.0, 2.0,\
      0.0,     0.0,     0.0,    -2.0, 0.0,};
    return std::complex<double>(0,1./2.0)*uni10::Matrix(dim, dim, mat_elem);
  }
  return uni10::Matrix();
}

uni10::Matrix Operator::matSz(float spin){
  spin_check(spin);
  int dim = spin * 2 + 1;
	/*
  if(dim > CURRENT_SUPPORTED_SPIN_DIM){
    std::ostringstream err;
    err<<"Spin = "<<spin <<" is not yet supported.";
    throw std::runtime_error(err.str());
  }
	*/
  if(dim == 2){
    double mat_elem[] = {\
      0.5,  0,\
      0,   -0.5  };
    return uni10::Matrix(dim, dim, mat_elem);
  }
  else if(dim == 3){
    double mat_elem[] = {\
      1.0, 0.0,  0.0,\
      0.0, 0.0,  0.0,\
      0.0, 0.0, -1.0};
    return uni10::Matrix(dim, dim, mat_elem);
  }
  else if(dim == 5){
    double mat_elem[] = {\
      2.0, 0.0, 0.0, 0.0, 0.0,\
      0.0, 1.0, 0.0, 0.0, 0.0,\
      0.0, 0.0, 0.0, 0.0, 0.0,\
      0.0, 0.0, 0.0,-1.0, 0.0,\
      0.0, 0.0, 0.0, 0.0,-2.0,};
    return uni10::Matrix(dim, dim, mat_elem);
  }
  return uni10::Matrix();
}

std::map<std::string, uni10::UniTensor> Operator::getOpts(){
  return opts;
}
