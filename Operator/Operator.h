#ifndef OPERATOR
#define OPERATOR

#include "uni10.hpp"
#include <map>

class Operator{
	
	public:
		Operator(){};
		Operator(const char* opts, float spin=0.5, int sites=2);
		uni10::Matrix matSx(float spin=0.5);
		uni10::Matrix matSy(float spin=0.5);
		uni10::Matrix matSp(float spin);
		uni10::Matrix matSm(float spin);
		uni10::Matrix matSz(float spin=0.5);
		std::map<std::string, uni10::UniTensor> getOpts();
		~Operator(){};

	private:
		void spin_check(float spin);
		std::map<std::string, uni10::UniTensor>	opts;
	
	/*
	uni10::UniTensor periodicSx(size_t siteNum, float spin = 0.5);
	uni10::UniTensor periodicSy(size_t siteNum, float spin = 0.5);
	uni10::UniTensor periodicSz(size_t siteNum, float spin = 0.5);
	uni10::UniTensor periodicsqSx(size_t siteNum, float spin = 0.5);
	uni10::UniTensor periodicsqSy(size_t siteNum, float spin = 0.5);
	uni10::UniTensor periodicsqSz(size_t siteNum, float spin = 0.5);
	*/

};

#endif
