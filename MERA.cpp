#include <iostream>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <algorithm>
#include <numeric>
#include "MERA.h"

MERA::MERA(int rank, int chi, int dim, int layers, double epsilon, int q_tensor, int q_layer)
    :rank(rank), chi(chi), dim(dim), layers(layers),
     lattices(layers+1), sites(2*(int)pow(3,layers-1)), epsilon(epsilon),
     q_tensor(q_tensor), q_layer(q_layer),
     disentangler(new UniTensor[layers-1]),
     coarsegrainer(new UniTensor[layers-1]),
     hamiltonian(new UniTensor[layers]),
     densitymatrix(new UniTensor[layers]),
     sigmaz(new UniTensor[layers])
{
    {// disentangler
        vector<Bond> bdb(4, Bond(BD_IN, dim)); // bottom disentangler
        bdb[2].assign(BD_OUT, chi);
        bdb[3].assign(BD_OUT, chi);
        UniTensor utb(CTYPE, bdb, "[0]");
        utb.orthoRand(CTYPE);
        disentangler[0] = utb;
        vector<Bond> bd(4, Bond(BD_IN, chi));
        bd[2].assign(BD_OUT, chi);
        bd[3].assign(BD_OUT, chi);
        for (int i=1; i<layers-1; i++){
            char buf[5]; sprintf(buf, "%d", i);
            UniTensor ut(CTYPE, bd, "["+(string)buf+"]");
            ut.orthoRand(CTYPE);
            disentangler[i] = ut;
        }
        //for (int i=0; i<layers-1; i++)
        //    cout << disentangler[i] << endl;
    }
    {// toptensor and coarsegrainer
        vector<Bond> bdt(3, Bond(BD_IN, chi)); // top tensor bonds
        bdt[2].assign(BD_OUT, rank);
        UniTensor utt(CTYPE, bdt, "[top]");
        utt.orthoRand(CTYPE); // make sure utt^dag * utt = I, 
                              // and tr(utt^dag * utt) = rank
        toptensor = utt;
        vector<Bond> bdb(4, Bond(BD_IN, chi)); // bottom coarsegrainer
        bdb[1].assign(BD_IN, dim); bdb[3].assign(BD_OUT, chi);
        UniTensor utb(CTYPE, bdb, "[0]");
        utb.orthoRand(CTYPE);
        coarsegrainer[0] = utb;
        vector<Bond> bd(4, Bond(BD_IN, chi));
        bd[3].assign(BD_OUT, chi);
        for (int i=1; i<layers-1; i++){
             char buf[5]; sprintf(buf, "%d", i);
             UniTensor ut(CTYPE, bd, "["+(string)buf+"]");
             ut.orthoRand(CTYPE);
             coarsegrainer[i] = ut;
        }
        //cout << toptensor << endl;
        //for (int i=0; i<layers-1; i++)
        //    cout << coarsegrainer[i] << endl;
    }
    {// utility matrix
        assert(dim == 2 && "Support spin-half systems only");
        double ePz[] = {1., 0., 0., -1.};
        double ePx[] = {0., 1., 1., 0.};
        double eSp[] = {0., 1., 0., 0.};
        double eSm[] = {0., 0., 1., 0.};
        Matrix mPz(dim, dim, ePz); Pz = mPz;
        Matrix mPx(dim, dim, ePx); Px = mPx;
        Matrix mSp(dim, dim, eSp); Sp = mSp;
        Matrix mSm(dim, dim, eSm); Sm = mSm;
        Matrix mIdent(dim, dim);   
        mIdent.identity(); Ident = mIdent;
    }
    {// empty hamiltonian and densitymatrix
        UniTensor ut;
        for (int i=0; i<layers; i++){
            hamiltonian[i] = ut;
            densitymatrix[i] = ut;
        }
     // initialize densitmatrix[layers-1] from top tensor
        UniTensor top = toptensor;
        UniTensor topT = top; topT.cTranspose();
        int lb[] = {2, 3, 4};
        int lbo[] = {0, 1, 3, 4};
        topT.setLabel(lb);
        UniTensor Dens = topT * top;
        Dens.permute(lbo, 2);
        densitymatrix[layers-1] = Dens;
    }
    {// sigmaz
        Matrix sigz = 1.0/2.0*otimes(Pz, Ident) + 1.0/2.0*otimes(Ident, Pz);
        vector<Bond> bd(4, Bond(BD_OUT, dim));
        bd[0].assign(BD_IN, dim);
        bd[1].assign(BD_IN, dim);
        UniTensor ut(bd, "[0]");
        ut.putBlock(sigz);
        sigmaz[0] = ut;
    }
}


MERA::~MERA(){
    delete [] disentangler;
    delete [] coarsegrainer;
    delete [] hamiltonian;
    delete [] densitymatrix;
}


void MERA::SetIsing(double lamb){
    Matrix ham = -1.0*otimes(Pz, Pz) +-1.0*otimes(lamb/2.*Px, Ident)
               +-1.0*otimes(Ident, lamb/2.*Px);
    vector<Bond> bd(4, Bond(BD_OUT, dim));
    bd[0].assign(BD_IN, dim); 
    bd[1].assign(BD_IN, dim);
    UniTensor ut(bd, "[0]");
    ut.putBlock(ham);
    hamiltonian[0] = ut;
    //cout << hamiltonian[0] << endl;
}


void MERA::SetHeisenberg(){
    Matrix ham = (otimes(0.5*Pz, 0.5*Pz) + 0.5*(otimes(Sp, Sm) + otimes(Sm, Sp)));
    vector<Bond> bd(4, Bond(BD_OUT, dim));
    bd[0].assign(BD_IN, dim); 
    bd[1].assign(BD_IN, dim);
    UniTensor ut(bd, "[0]");
    ut.putBlock(ham);
    hamiltonian[0] = ut;
    //cout << hamiltonian[0] << endl;
}


UniTensor MERA::Ascend(int tau, const UniTensor& Obs){
    UniTensor WL = coarsegrainer[tau];
    UniTensor WR = WL;
    UniTensor WLT = WL; WLT.cTranspose();
    UniTensor WRT = WR; WRT.cTranspose();
    UniTensor U = disentangler[tau];
    UniTensor UT = U; UT.cTranspose();

    map<string, UniTensor*> tens;
    tens["WL"] = &WL;
    tens["WR"] = &WR;
    tens["U"] = &U;
    tens["Obs"] = const_cast<UniTensor*>(&Obs);
    tens["UT"] = &UT;
    tens["WLT"] = &WLT;
    tens["WRT"] = &WRT;

    Network ascL("./Diagrams/AscendL");
    Network ascC("./Diagrams/AscendC");
    Network ascR("./Diagrams/AscendR");
    for (map<string, UniTensor*>::iterator it=tens.begin(); it!=tens.end(); it++){
        ascL.putTensor(it->first, it->second);
        ascC.putTensor(it->first, it->second);
        ascR.putTensor(it->first, it->second);
    }


    UniTensor ascObs = ascL.launch();
    ascObs += ascC.launch();
    ascObs += ascR.launch();
    ascObs *= 1.0 / 3.0;

    return ascObs;
}


UniTensor MERA::Descend(int tau, const UniTensor& Dens){
    UniTensor WL = coarsegrainer[tau-1];
    UniTensor WR = WL;
    UniTensor WLT = WL; WLT.cTranspose();
    UniTensor WRT = WR; WRT.cTranspose();
    UniTensor U = disentangler[tau-1];
    UniTensor UT = U; UT.cTranspose();
    
    map<string, UniTensor*> tens;
    tens["WL"] = &WL;
    tens["WR"] = &WR;
    tens["U"] = &U;
    tens["Dens"] = const_cast<UniTensor*>(&Dens);
    tens["UT"] = &UT;
    tens["WLT"] = &WLT;
    tens["WRT"] = &WRT;

    Network desL("./Diagrams/DescendL");
    Network desC("./Diagrams/DescendL");
    Network desR("./Diagrams/DescendR");
    
    for (map<string, UniTensor*>::iterator it=tens.begin(); it!=tens.end(); it++){
        desL.putTensor(it->first, it->second);
        desC.putTensor(it->first, it->second);
        desR.putTensor(it->first, it->second);
    }

    UniTensor desDens = desL.launch();
    desDens += desC.launch();
    desDens += desR.launch();
    desDens *= 1.0 / 3.0;

    return desDens;
}


void MERA::OptW(int tau){
    for (int q=0; q<q_tensor; q++)
    {
    UniTensor Dens = densitymatrix[tau+1];
    UniTensor Obs = hamiltonian[tau];
    UniTensor W = coarsegrainer[tau];
    UniTensor WLT = W; WLT.cTranspose();
    UniTensor WRT = W; WRT.cTranspose();
    UniTensor U = disentangler[tau];
    UniTensor UT = U; UT.cTranspose(); 
    
    map<string, UniTensor*> tens;
    tens["Dens"] = &Dens;
    tens["W"] = &W;
    tens["U"] = &U;
    tens["Obs"] = &Obs;
    tens["UT"] = &UT;
    tens["WLT"] = &WLT;
    tens["WRT"] = &WRT;

    Network WLEnvL("./Diagrams/WLEnvL");
    Network WLEnvC("./Diagrams/WLEnvC");
    Network WLEnvR("./Diagrams/WLEnvR");
    Network WREnvL("./Diagrams/WREnvL");
    Network WREnvC("./Diagrams/WREnvC");
    Network WREnvR("./Diagrams/WREnvR");
    for (map<string, UniTensor*>::iterator it=tens.begin(); it!=tens.end(); it++){
        WLEnvL.putTensor(it->first, it->second);
        WLEnvC.putTensor(it->first, it->second);
        WLEnvR.putTensor(it->first, it->second);
        WREnvL.putTensor(it->first, it->second);
        WREnvC.putTensor(it->first, it->second);
        WREnvR.putTensor(it->first, it->second);
    }

    UniTensor env = WLEnvL.launch();
    env += WLEnvC.launch();
    env += WLEnvR.launch();
    env += WREnvL.launch();
    env += WREnvC.launch();
    env += WREnvR.launch();

    vector<Matrix> svd = env.getBlock().svd();
    coarsegrainer[tau].putBlock(-1.0*(svd[0] * svd[2]).cTranspose());
    }
}


void MERA::OptU(int tau){
    for (int q=0; q<q_tensor; q++)
    {
    UniTensor Dens = densitymatrix[tau+1];
    UniTensor Obs = hamiltonian[tau];
    UniTensor WL = coarsegrainer[tau];
    UniTensor WLT = WL; WLT.cTranspose();
    UniTensor WR = WL;
    UniTensor WRT = WR; WRT.cTranspose();
    UniTensor UT = disentangler[tau]; UT.cTranspose(); 

    map<string, UniTensor*> tens;
    tens["Dens"] = &Dens;
    tens["WL"] = &WL;
    tens["WR"] = &WR;
    tens["Obs"] = &Obs;
    tens["UT"] = &UT;
    tens["WLT"] = &WLT;
    tens["WRT"] = &WRT;
    
    Network UEnvL("./Diagrams/UEnvL");
    Network UEnvC("./Diagrams/UEnvC");
    Network UEnvR("./Diagrams/UEnvR");
    for (map<string, UniTensor*>::iterator it=tens.begin(); it!=tens.end(); it++){
        UEnvL.putTensor(it->first, it->second);
        UEnvC.putTensor(it->first, it->second);
        UEnvR.putTensor(it->first, it->second);
    }

    UniTensor env = UEnvL.launch();
    env += UEnvC.launch();
    env += UEnvR.launch();
    
    vector<Matrix> svd = env.getBlock().svd();
    disentangler[tau].putBlock(-1.0*(svd[0] * svd[2]).cTranspose());
    }
}


void MERA::Optimize(){
    double p_sum; p_sum = 0.0;
    double n_sum; n_sum = 1.0;
    int iter = 0;

    while (abs(n_sum - p_sum) > epsilon)
    {
    for (int l=layers-1; l>1; l--){
        // A1
        densitymatrix[l-1] = Descend(l, densitymatrix[l]);
    }
    for (int l=0; l<layers-1; l++){
        // A2
        for (int q=0; q<q_layer; q++){
            OptU(l);
            OptW(l);
        }
        // A3
        hamiltonian[l+1] = Ascend(l, hamiltonian[l]);
    }
    // A4
    vector<Matrix> eig = hamiltonian[layers-1].getBlock().eig();
    vector<pair<size_t,double> > eigvalue;
    for (size_t i=0; i<eig[0].elemNum(); i++){
        eigvalue.push_back(make_pair(i, eig[0](i).real()));
    }
    std::sort(eigvalue.begin(), eigvalue.end(), comparator);
    vector<double> energy;
    for (int i=0; i<rank; i++){
        energy.push_back(eigvalue[i].second);
        for (int j=0; j<chi*chi; j++)
            toptensor.getBlock()(i + rank*j) = eig[1](chi*chi*eigvalue[i].first + j);
    }

    UniTensor top = toptensor;
    UniTensor topT = top; topT.cTranspose();
    int lb[] = {2, 3, 4};
    int lbo[] = {0, 1, 3, 4};
    topT.setLabel(lb);
    UniTensor Dens = topT * top;
    Dens.permute(lbo, 2);
    densitymatrix[layers-1] = Dens;

    double ground; ground = eigvalue[0].second;
    p_sum = n_sum;
    n_sum = std::accumulate(energy.begin(), energy.end(), 0.0);

    iter++;
    printf("iter = %4d, E/N = %14.10f, e = %9.4e\n", iter, ground, abs(n_sum-p_sum));
    //cout << "iter = " << iter << ", E/N = " << ground << ", e = " << abs(n_sum-p_sum) << endl;
    }
    
    // compute spontaneous magnetization sigmaz
    for (int l=0; l<layers-1; l++)
        sigmaz[l+1] = Ascend(l, sigmaz[l]);
    Matrix gs = toptensor.getBlock(); gs.resize(chi*chi, 1);
    Matrix gsT = gs; gsT.cTranspose();
    Matrix expectsigmaz = gsT * sigmaz[layers-1].getBlock() * gs;
    cout << "Spontaneous magnetization = " << expectsigmaz(0).real() << endl;
}


void MERA::Test(){
    {// test Ascend
        //cout << Ascend(0, hamiltonian[0]) << endl;
    }

    {// test Descend
        //cout << Descend(layers-1, Dens) << endl;
    }
    {// test OptW
        /*
        cout << coarsegrainer[0];
        for (int i=layers-1; i>1; i--){
            densitymatrix[i-1] = Descend(i, densitymatrix[i]);
        }
        OptW(0);
        cout << coarsegrainer[0];
        */
    }
    {// test OptU
        /*
        cout << disentangler[0];
        for (int i=layers-1; i>1; i--){
            densitymatrix[i-1] = Descend(i, densitymatrix[i]);
        }
        OptU(0);
        cout << disentangler[0];
        */
    }
}





