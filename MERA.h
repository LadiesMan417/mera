#ifndef MERA_H
#define MERA_H

#include <vector>
#include <string>
#include <complex>
#include <utility>
using namespace std;
#include <uni10.hpp>
using namespace uni10;

class MERA{
    private:
        int rank;       // contravariant dimension of top tensor
        int chi;        // dominant bond dimension
        int dim;        // physical dimension
        int layers;     // number of layers (tau), 0, 1, .., layers-1
                        // this is where the disentangler and coarsegrainer live
        int lattices;   // indices of layer of lattice, 0, 1, .., layers
                        // this is where the hamiltonian and densitymatrix live
        int sites;      // number of sites, equal 2*3^(layers-1)
        double epsilon; // convergence criterion
        int q_tensor;
        int q_layer;

        UniTensor* disentangler;    // unitary
        UniTensor* coarsegrainer;   // isometry
        UniTensor  toptensor;
        UniTensor* hamiltonian;
        UniTensor* densitymatrix;
        UniTensor* sigmaz;          // spontaneous magnetization

        Matrix Pz;
        Matrix Px;
        Matrix Sp;
        Matrix Sm;
        Matrix Ident;
        static bool comparator (const pair<size_t,double>& l, const pair<size_t,double>& r){
            return l.second < r.second;
        }

    public:
        MERA(int dim, int rank, int chi, int layers, double epsilon, int q_tensor, int q_layer);
        ~MERA();
        
        void SetIsing(double lamb);
        void SetHeisenberg();
        
        UniTensor Ascend(int tau, const UniTensor& Obs); 
        UniTensor Descend(int tau, const UniTensor& Dens);
        void OptW(int tau);
        void OptU(int tau);
        void Optimize();
        double Expectation();

        void Test();

};
#endif
